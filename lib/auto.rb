class Auto
    
    
    def initialize()
        @posXinicial = 0
        @posYinicial = 0
        @orientacioninicial = 'N'
        @comandos = ''
        
    end
    
    def cargardatos(x ,y , o, c)
        @posXinicial = x
        @posYinicial = y
        @orientacioninicial = o
        @comandos = c
       
        
    end

    def getPosX()
        return @posXinicial        
    end

    def getPosY()
        return @posYinicial        
    end

    def getDireccion()
        return @orientacioninicial        
    end

    def getComandos()
        return @comandos        
    end

        
    def calcularPosFinal()
        
        for i in(0.. @comandos.length )
            if @comandos[i]=='I'
                
                if @orientacioninicial == 'N' 
                    @orientacioninicial = 'O'
                 
                elsif @orientacioninicial == 'O' 
                    @orientacioninicial = 'S'
                 
                elsif @orientacioninicial == 'S' 
                    @orientacioninicial ='E'
                 
                elsif @orientacioninicial == 'E' 
                     @orientacioninicial ='N'
                end 
            end 

            if @comandos[i] == 'D' 
               
                    if @orientacioninicial == 'N'  
                        @orientacioninicial ='E'     
                    
                    elsif @orientacioninicial == 'E'
                        @orientacioninicial ='S'
                     
                    elsif @orientacioninicial == 'S' 
                        @orientacioninicial ='O'
                     
                    elsif @orientacioninicial == 'O'
                        @orientacioninicial ='N'
                    end

            end 

            if @comandos[i]=='A'
                if @orientacioninicial == 'N' 
                    @posYinicial = @posYinicial.to_i + 1
                 
                 elsif @orientacioninicial == 'E' 
                    @posXinicial = @posXinicial.to_i + 1
                 
                 elsif @orientacioninicial == 'S' 
                    @posYinicial = @posYinicial.to_i - 1
                 
                  elsif @orientacioninicial == 'O' 
                    @posXinicial = @posXinicial.to_i - 1
                end 
            end 

            i+=1
        end 
        @vec = [@posXinicial, @posYinicial, @orientacioninicial]
        return @vec.to_a
    end



end

