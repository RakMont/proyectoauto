require './lib/auto'

RSpec.describe Auto do
    it'Deberia retornar posicion inicial en X' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AIDAID")
        expect(auto1.getPosX()).to eq 4
    end

    it'Deberia retornar posicion inicial en Y' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AIDAID")
        expect(auto1.getPosY()).to eq 3
    end

    it'Deberia retornar orientacion inicial en Y' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AIDAID")
        expect(auto1.getDireccion()).to eq "N"
    end

    it'Deberia retornar comandos' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AIDAID")
        expect(auto1.getComandos()).to eq "AIDAID"
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","A")
        expect(auto1.calcularPosFinal()).to match_array([4,4,"N"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","D")
        expect(auto1.calcularPosFinal()).to match_array([4,3,"E"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","I")
        expect(auto1.calcularPosFinal()).to match_array([4,3,"O"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AI")
        expect(auto1.calcularPosFinal()).to match_array([4,4,"O"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AID")
        expect(auto1.calcularPosFinal()).to match_array([4,4,"N"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AIDA")
        expect(auto1.calcularPosFinal()).to match_array([4,5,"N"])
    end
  
    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AIDAD")
        expect(auto1.calcularPosFinal()).to match_array([4,5,"E"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","AIDADA")
        expect(auto1.calcularPosFinal()).to match_array([5,5,"E"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","DDDDDDD")
        expect(auto1.calcularPosFinal()).to match_array([4,3,"O"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"N","IIIIII")
        expect(auto1.calcularPosFinal()).to match_array([4,3,"S"])
    end

    it'Deberia retornar resultado' do
        auto1 = Auto.new
        auto1.cargardatos(4,3,"O","AIDADA")
        expect(auto1.calcularPosFinal()).to match_array([2,4,"N"])
    end

    
    

end