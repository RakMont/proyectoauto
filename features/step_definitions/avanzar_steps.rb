Given("visito la pagina de ingresar Comandos") do
    visit '/ingresarComandos'
end

Given ("ingreso la longitud  X de {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end


Given ("ingreso la longitud  Y de {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end

Given ("ingreso la orientacion Para {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end
Given ("ingreso como Avanzar o Derecha o Izquierda {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end

When  ("presiono el boton para obtener PosicioActual {string}") do |boton|
    click_button(boton)
end

Then("deberia ver la nueva Posicion de X como {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end

Then("deberia ver la nueva Posicion de Y como {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end
Then("deberia ver la nueva Orientacion  como {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end



 