Given("visito la pagina de ingresar Parametro") do
    visit '/parametros'
end

Given ("ingreso como largo {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end
Given ("ingreso como alto {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end

When  ("presiono el boton para enviar {string}") do |boton|
    click_button(boton)
end

Then("deberia ver el nuevo parametro de largo {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end

Then("deberia ver el nuevo parametro de alto {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end

  