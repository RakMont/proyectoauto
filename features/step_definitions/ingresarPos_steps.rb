Given ("visito el formulario de ingresar Posicion Inicial") do
    visit '/ingresarPosicion'
end

Given ("ingreso la longitud para X de {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end

  

Given ("ingreso la longitud para Y de {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end
Given ("ingreso la orientacion de {string} en el campo {string}")do |valor,campo|
    fill_in(campo, :with => valor)

end
    

When  ("presiono el boton {string} Para Registrar") do |boton|
    click_button(boton)
end

Then("deberia ver la posicion X como {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end



Then("deberia ver la posicion Y como {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end

Then("deberia ver la orientacion como {string}") do |mensaje|
    last_response.body.should =~ /#{mensaje}/m
end
