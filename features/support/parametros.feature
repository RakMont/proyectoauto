Feature: 
    Como Jugador
    Quiero Ingresar los Parametros del terreno
    Para decidir yo el tamanio

Scenario: 
    Given visito la pagina de ingresar Parametro
    And ingreso como largo "10" en el campo "largo"
    And ingreso como alto "5" en el campo "alto"
    When presiono el boton para enviar "RegistrarParametros"
    Then deberia ver el nuevo parametro de largo "X=10"
    Then deberia ver el nuevo parametro de alto "Y=5"

 