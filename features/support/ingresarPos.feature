Feature: 
    Como Jugador
    Quiero ingresar una posicion inicial
    Para decidir donde empezar el juego

Scenario: 
    Given visito el formulario de ingresar Posicion Inicial
    And ingreso la longitud para X de "5" en el campo "PosX"
    And ingreso la longitud para Y de "4" en el campo "PosY" 
    And ingreso la orientacion de "N" en el campo "orientacion"
    When presiono el boton "RegistrarDatos" Para Registrar
    Then deberia ver la posicion X como "PosX= 5"
    Then deberia ver la posicion Y como "PosY= 4"
    Then deberia ver la orientacion como "orientacion= N"
    
