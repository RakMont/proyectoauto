#outside + in =>cucumber a tdd
#inside - out =>  from TDD  a cucumber
require 'sinatra'
require './lib/auto'
get '/' do
    erb:index
end
get '/parametros' do
    erb:parametros
end

get '/saludo' do
    erb:saludo
end
post '/saludar' do
    @nombre_persona= params[:nombre]
    erb:saludo_nombre
end

post '/registrarParametros' do
    @largo= params[:largo]
    @alto= params[:alto]

    erb:Par_registrados
end

get '/ingresarPosicion' do
    @largo= params[:largo]
    @ancho= params[:ancho]
    @PosX= params[:PosX]
    @PosY= params[:PosY]
    @orientacion= params[:orientacion]
    @comandos=params[:comandos]


    erb:ingresarPosicion
end

post '/ingresarPos' do

    @largo= params[:largo]
    @ancho= params[:ancho]
    @PosX= params[:PosX]
    @PosY= params[:PosY]
    @orientacion= params[:orientacion]
    @comandos=params[:comandos]
    @res=params[:res]

    auto1 = Auto.new
    auto1.cargardatos(@PosX, @PosY, @orientacion, @comandos)
    @res = auto1.calcularPosFinal()
    
    erb:mostrarPosicion

    
end



get '/resultado' do

    @largo= params[:largo]
    @ancho= params[:ancho]
    @PosX= params[:PosX]
    @PosY= params[:PosY]
    @orientacion= params[:orientacion]
    @comandos=params[:comandos]
    auto1 = Auto.new
    erb:resultado

    
end



get '/ingresarComandos' do
    erb:ingresarComandos
end

post '/mostrarResultado' do

    @PosX= params[:PosX]
    @PosY= params[:PosY]
    @orientacion= params[:orientacion]
    @comandos=params[:comandos]

    
    erb:mostrarResultado
end


